class Transaction < ApplicationRecord
  belongs_to :user

  enum transaction_type: [ :Expense, :Enter ]

  validates :transaction_type, presence: true
  validates :amount, presence: true
end
