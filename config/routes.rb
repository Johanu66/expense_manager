Rails.application.routes.draw do
  resources :transactions, only: %i[ index new create edit update destroy ]
  devise_for :users
  root to: "home#index"
  get "balance_sheet", to: "transactions#balance_sheet", as: "balance_sheet"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
