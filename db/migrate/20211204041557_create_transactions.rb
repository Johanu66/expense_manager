class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.integer :transaction_type, null: false, default: 0
      t.integer :amount, null: false
      t.date :transaction_date
      t.text :motif
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
